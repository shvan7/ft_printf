#ifndef LIBFT_H

#define LIBFT_H

#include <string.h>
#include <stdlib.h>

int ft_int_exist(int value, int *tab, size_t n);
int **ft_tabnew(int column, int nb);

void ft_swap(char *a, char *b);
char *ft_strrev(char *str);

void *ft_memset(void *s, int c, size_t n);
void ft_bzero(void *s, size_t n);
void *ft_memcpy(void *dest, const void *src, size_t n);
void *ft_memccpy(void *dest, const void *src, int c, size_t n);
void *ft_memmove(void *dest, const void *src, size_t n);
int ft_memcmp(const void *s1, const void *s2, size_t n);
void *ft_memchr(const void *s, int c, size_t n);
void *ft_memalloc(size_t size);
void ft_memdel(void **ap);

size_t ft_strlen(const char *s);
const char *ft_strskip(char const *s, char c);
int ft_strcspn(char const *s, char const *charset);
char *ft_strdup(const char *s);
char *ft_strcpy(char *dest, const char *src);
char *ft_strncpy(char *dest, const char *src, size_t n);
char *ft_strcat(char *dest, const char *src);
char *ft_strncat(char *dest, const char *src, size_t n);
size_t ft_strlcat(char *dest, const char *src, size_t n);
char *ft_strchr(const char *s, int c);
char *ft_strrchr(const char *s, int c);
char *ft_strstr(const char *str, const char *find);
char *ft_strnstr(const char *str, const char *find, size_t n);
int ft_strcmp(const char *s1, const char *s2);
int ft_strncmp(const char *s1, const char *s2, size_t n);
int ft_tokenlen(char const *s, char c);

char *ft_itoa(int n);
int ft_atoi(const char *str);
int ft_countdec(int nb);
size_t ft_power(size_t n, int p);
size_t ft_abs(int n);
void ft_putnbr_base(int nbr, char *base);
int ft_atoi_base(char *str, char *base);
char *ft_convert_base(char *nbr, char *base_from, char *base_to);

int ft_istrim(int c);
int ft_isalpha(int c);
int ft_isdigit(int c);
int ft_isalnum(int c);
int ft_isascii(int c);
int ft_isprint(int c);
int ft_toupper(int c);
int ft_tolower(int c);
int ft_isupper(int c);
int ft_islower(int c);
int ft_issign(int c);
int ft_sqrt(int nb);

char *ft_strnew(size_t size);
void ft_strdel(char **as);
void ft_strclr(char *s);
void ft_striter(char *s, void (*f)(char *));
void ft_striteri(char *s, void (*f)(unsigned int, char *));
char *ft_strmap(char const *s, char (*f)(char));
char *ft_strmapi(char const *s, char (*f)(unsigned int, char));
int ft_strequ(char const *s1, char const *s2);
int ft_strnequ(char const *s1, char const *s2, size_t n);
char *ft_strsub(char const *s, unsigned int start, size_t len);
char *ft_strjoin(char const *s1, char const *s2);
char *ft_strtrim(char const *s);
char **ft_strsplit(char const *s, char c);

void ft_putchar(char c);
void ft_putchar_fd(char c, int fd);
void ft_putstr(char const *s);
void ft_putstr_fd(char const *s, int fd);
void ft_putendl(char const *s);
void ft_putendl_fd(char const *s, int fd);
void ft_putnbr(int n);
void ft_putnbr_fd(int n, int fd);

int ft_remchar(char *str, char c, size_t n);

#endif