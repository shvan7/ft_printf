#ifndef FT_PRINTF_H

# define FT_PRINTF_H
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>
# include <float.h>
# include <limits.h>
# include <math.h>
# include "libft.h"

// FLAG OUTPUT
// c, s, p, d, i, o, u, x, X, f, %, e, g, b, r, k

// FLAG INPUT
# define F_INPUT ("hlL")
// h, hh, l, L, ll

// FLAG SPE
#define F_SPEC ("0#-+ *$'")
// #, 0, -, +, esp, *, $, '

typedef struct s_map
{
  // a parser
  char output;
  char *input;

  // info nombre
  int padding;
  int precision;
  
  // infos
  int position;
  int info[2]; // info[0] => index debut - info[1] => index fin
  
  // expr = rendu
  char *expr;
  char *prefix;
  char *render;
  
  // navigation
  struct s_map *next;
  struct s_map *prev;
}              t_map;

// FUNC
// ATTRIBUTE
// __attribute__((format()))

int ft_printf(const char *format, ...);

// PARSER
int ft_check_end_expr(char c);
int ft_nb_expr(const char *format);
int ft_expr_index_star(const char *format, int index);
int ft_expr_index_end(const char *format, int index);
void ft_expr_fill(t_map *data, const char *format);

// MEMORY
void ft_star_of_list(t_map **data);
void ft_lstadd_end(t_map **alst, t_map *new);
void ft_clean_links_list(t_map *data);
t_map *ft_build_links_list(const char *format, int position);
t_map *ft_build_lst(const char *format);

// ===================================================== GET

// GET INFO
void ft_get_flag_input(t_map *data);
void ft_get_flag_output(t_map *data);
void ft_get_padding(t_map *data, va_list va);
void ft_get_precision(t_map *data, va_list va);

// GET ARG
long long ft_get_arg_int(va_list va, int position);
unsigned long long ft_get_arg_uint(va_list va, int position);
void *ft_get_arg_adress(va_list va, int position);
char *ft_get_arg_str(va_list va, int position);
unsigned char *ft_get_arg_ustr(va_list va, int position);
double ft_get_arg_double(va_list va, int position);
long double ft_get_arg_ldouble(va_list va, int position);

// GET POSITION
void ft_update_positon(t_map *data);

// GET PRECISION
void ft_update_precision(t_map *data, va_list va);

// GET PADDING
void ft_update_padding(t_map *data, va_list va);

// ===================================================== PROCESS

// PROCESS PRECISION
void ft_add_precision(t_map *data);

// PROCESS PADDING
void ft_add_padding(t_map *data);

// PROCESS NUMBER
void ft_print_d(t_map *data, long long d);

// PROCESS UNSIGNED NUMBER
void ft_print_u(t_map *data, unsigned long long d);

// PROCESS STRING
void  ft_print_char(t_map *data, char c);

// PROCESS OCTAL
void ft_print_o(t_map *data, unsigned long long nb);

// PROCESS HEXA
void ft_print_x(t_map *data, unsigned long long nb, int boolean);

// PROCESS ADRESS
void ft_print_adress(t_map *data, void *p0);

// PROCESS BINARY
void ft_print_bin(t_map *data, long long nb);

// PROCESS E
void ft_print_e(t_map *data, long double nb);

// ===================================================== RENDER

// RENDER
void ft_process_render(t_map *data, va_list va);

// ===================================================== UTILIS

// UTILIS LST
void ft_forlst(t_map *data, void (*f)(t_map *e));
void ft_forlsts(t_map *data, void (*f)(t_map *e));
void ft_forlsti(t_map *data, int a, void (*f)(t_map *e, int a));
void ft_forlstm(t_map *data, const char *format, void (*f)(t_map *e, const char *format));
void ft_forlst_arg(t_map *data, va_list va, void (*f)(t_map *e, va_list va));

// UTILIS
char *ft_strinser(char *str, char *cplm, size_t n);
void ft_offset_position(t_map *data);
void ft_offset_position_i(t_map *data, int a);
int ft_str_pattern(const char *str, const char *find);

// DISPLAY
int ft_display(t_map *data, const char *format);
void ft_display_links(t_map *data);

#endif