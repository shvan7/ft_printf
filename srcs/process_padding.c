#include "ft_printf.h"

static void ft_str_prefix(t_map *data, char c) // --------------------- CREATE PREFIX
{
  int diff;

  data->padding = ft_abs(data->padding);
  diff = data->padding - ft_strlen(data->render);
  if (!(data->prefix = ft_strnew(diff)))
    return ;
  while (diff--)
    data->prefix[diff] = c;
}

static int ft_check_zero(char *expr) // CHECK SPEC ZERO
{
  int i_zero;

  i_zero = ft_strcspn(expr, ".0");
  if (expr[i_zero] == '.')
    return (0);
  if (i_zero > -1 && !ft_isdigit(expr[i_zero - 1]))
    return (1);
  return (0);
}

static void ft_add_prefix(t_map *data, char c, int i) // ------------- CREATE PREFIX && INSERT
{
  char *tmp;

  tmp = data->render;
  ft_str_prefix(data, c); // recup prefix
  data->render = ft_strinser(data->render, data->prefix, i); // pas de return car free dans tous les cas
  if (data->render != tmp) // free que si adress change
    ft_strdel(&tmp);
}

void ft_add_padding(t_map *data) // ---------------------------------- CORE
{
  int i; // index du debut de insertion prefix
  char c; // valeur du prefix
  int size;

  size = ft_strlen(data->render);
  c = ft_check_zero(data->expr) ? '0' : ' ';
  if (!data->padding || size >= (int)ft_abs(data->padding)) // si padding null ou plus petit que render stop
    return ;
    i = 0;
  if (data->padding < 0)  // padding neg desactive le '0'
  {
    c = ' ';
    i = ft_strlen(data->render);
  }
  // STRING
  if (data->output == 's') // string pas de zero
    c = ' ';
  // HEXA
  else if ((data->output == 'x' || data->output == 'X') && c == '0' && !i) // if 0xfff
  {
    if (ft_strcspn(data->expr, "#") > -1) // appel func after for opti
      i = 2; // skip '0x'
  }
  ft_add_prefix(data, c, i);
}