#include "ft_printf.h"

static int  ft_check_csp(t_map *data, va_list va)
{
  int ret;

  ret = 0;
  if (data->output == 'c' && ++ret)
    ft_print_char(data, ft_get_arg_int(va, data->position));
  else if (data->output == 's' && ++ret)
    data->render = ft_strdup(ft_get_arg_str(va, data->position));
  else if (data->output == 'p' && ++ret)
    ft_print_adress(data, ft_get_arg_adress(va, data->position));
  return (ret);
}

static int ft_check_dioux(t_map *data, va_list va)
{
  int ret;

  ret = 0;
  if (data->output == 'd' && ++ret)
    ft_print_d(data, ft_get_arg_int(va, data->position));
  else if (data->output == 'i' && ++ret)
    ft_print_d(data, ft_get_arg_int(va, data->position));
  else if (data->output == 'o' && ++ret)
    ft_print_o(data, ft_get_arg_uint(va, data->position));
  else if (data->output == 'u' && ++ret)
    ft_print_u(data, ft_get_arg_uint(va, data->position));
  else if (data->output == 'x' && ++ret)
    ft_print_x(data, ft_get_arg_uint(va, data->position), 0);
  else if (data->output == 'X' && ++ret)
    ft_print_x(data, ft_get_arg_uint(va, data->position), 1);
  return (ret);
}

static int ft_check_bns(t_map *data, va_list va)
{
  int ret;

  ret = 0;
  if (data->output == 'b' && ++ret)
    ft_print_bin(data, ft_get_arg_int(va, data->position));
  else if (data->output == 'r' && ++ret) // char * non imprimable -- TODOS
    ft_print_d(data, ft_get_arg_int(va, data->position));
  else if (data->output == 'k' && ++ret) // date format ISO qlqcq -- TODOS
    ft_print_d(data, ft_get_arg_int(va, data->position));
  return (ret);
}

// si % decale les index des maillons suivants
static void ft_check_other(t_map *data)
{
  if (!(data->render = ft_strnew(1)))
    return ;
  data->render[0] = data->output;
  ft_forlsts(data, ft_offset_position);
  data->position = 0;
}

void ft_process_render(t_map *data, va_list va)
{
  if (ft_check_csp(data, va))
    return ;
  if (ft_check_dioux(data, va))
    return ;
  if (ft_check_bns(data, va))
    return ;
  ft_check_other(data);
}