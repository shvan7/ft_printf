#include "ft_printf.h"

void ft_update_padding(t_map *data, va_list va)
{
  int i;
  int nb;
  int boolean;

  i = -1;
  nb = 0;
  data->padding = 0;
  while (data->expr[++i]) // count all *
  {
    if (data->expr[i] == '*')
      data->padding++;
    if (data->expr[i] == '*' && data->expr[i - 1] != '.')
    {
      i++;
      boolean = 0;
      nb = ft_atoi(data->expr + i);     // recupe le nombre apres le *
      while (ft_isdigit(data->expr[i])) // skip le nb
        i++;
      if (data->expr[i] == '$')
        boolean = 1;
    }
  }
  data->padding = ft_get_arg_int(va, boolean ? nb : data->padding);
}

// recupere le padding .. si bool .. verifie si *
void ft_get_padding(t_map *data, va_list va)
{
  int tmp; // stock avant dernier valeur
  int boolean;
  int i;

  tmp = 0;
  boolean = 0;
  i = 0;
  while (data->expr[i])
  {
    while (data->expr[i++] && !(ft_isdigit(data->expr[i]))) // boucle tant que pas un nb
      if (data->expr[i] == '*')                             // si tu vois un * active la recup arg
        boolean++;
    if (i && data->expr[i] && data->expr[i - 1] != '.') // recup le nb si pas precision
      if ((tmp = ft_atoi(data->expr + i)))
        boolean = 0;                                   // si tu as trouve un nb desactive la recup
    while (data->expr[i] && ft_isdigit(data->expr[i])) // boucle tant que nb
      i++;
    if (data->expr[i] == '$')
      boolean++;
    tmp && (data->padding = tmp); // recup tmp
  }
  if (ft_strcspn(data->expr, "-") > -1) // si negatif
    data->padding = -data->padding;
  if (boolean)
    ft_update_padding(data, va); // check si un * apres
}