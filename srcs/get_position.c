#include "ft_printf.h"

// check format /$
static int ft_check_format_1(char *expr)
{
  int i;
  int j;
  int nb;

  i = -1;
  j = -1;
  nb = 0;
  while (expr[++j])
  {
    i = ft_str_pattern(expr + j, "/$");
    if (i > -1 && expr[i + j - 1] != '*')
      nb = ft_atoi(expr + i + j);
    while (ft_isdigit(expr[j]))
      j++;
  }
  return (nb);
}

// count '*' after '.' 
static int ft_check_format_2(char *expr)
{
  int i;
  int nb;

  i = 0;
  nb = 0;
  while (expr[i++])
    if (expr[i] == '*' && expr[i + 1] != '$')
      nb++;
  return (nb);
}

// check * => si pas de $
static int ft_check_format_3(char *expr)
{
  int i;
  int nb;

  i = -1;
  nb = 0;
  while (expr[++i])
    if (expr[i] == '*')
      nb++;
  return (nb);
}

void ft_update_positon(t_map *data)
{
  int nb;

  nb = 0;
  if (data->expr)
  {
    if (ft_strcspn(data->expr, "$") > -1)
      nb = ft_check_format_1(data->expr);
    nb += ft_check_format_2(data->expr);
    data->position += ft_check_format_3(data->expr);
    if (nb)
      ft_forlsti(data, nb, ft_offset_position_i);
  }
}