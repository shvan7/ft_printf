#include "ft_printf.h"

long long ft_get_arg_int(va_list va, int position)
{
  int i;
  long long value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, long long);
  value = va_arg(tmp, long long);
  va_end(tmp);
  return (value);
}

unsigned long long ft_get_arg_uint(va_list va, int position)
{
  int i;
  long long value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, unsigned long long);
  value = va_arg(tmp, unsigned long long);
  va_end(tmp);
  return (value);
}

void *ft_get_arg_adress(va_list va, int position)
{
  int i;
  void *value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, void *);
  value = va_arg(tmp, void *);
  va_end(tmp);
  return (value);
}

char *ft_get_arg_str(va_list va, int position)
{
  int i;
  char *value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, int);
  value = va_arg(tmp, char*);
  va_end(tmp);
  return (value);
}

unsigned char *ft_get_arg_ustr(va_list va, int position)
{
  int i;
  unsigned char *value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, int);
  value = va_arg(tmp, unsigned char *);
  va_end(tmp);
  return (value);
}

double ft_get_arg_double(va_list va, int position)
{
  int i;
  double value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, double);
  value = va_arg(tmp, double);
  va_end(tmp);
  return (value);
}

long double ft_get_arg_ldouble(va_list va, int position)
{
  int i;
  long double value;
  va_list tmp;

  va_copy(tmp, va);
  i = 0;
  while (++i < position)
    va_arg(tmp, long double);
  value = va_arg(tmp, long double);
  va_end(tmp);
  return (value);
}