#include "ft_printf.h"

void  ft_get_flag_input(t_map *data)
{
  int i;
  char *flag;
  char v;

  if (!(data->expr) || !(flag = ft_strnew(2)))
    return ;
  i = -1;
  while (data->expr[++i])
    if ((v = data->expr[i]) && ft_strcspn(F_INPUT, &v) != -1)
    {
      flag[0] = v;
      break ;
    }
  v = data->expr[++i];
  if (ft_strcspn(F_INPUT, &v) != -1)
  {
    if (flag[0] == v)
      flag[1] = v;
    else
      ft_strcpy(flag, "ll");
  }
  data->input = flag;
}

void  ft_get_flag_output(t_map *data)
{
  char *v;

  if (!(data->expr))
    return ;
  v = data->expr;
  data->output = v[ft_strlen(v) - 1];
}