#include "ft_printf.h"

void ft_update_precision(t_map *data, va_list va)
{
  int i;
  int j;

  j = 0;
  data->precision = 1;
  i = ft_strlen(data->expr);
  while (--i && data->expr[i] != '.') // trouve le dernier point
    ;
  j = ft_str_pattern(data->expr, ".*/$");
  if (j >= i)
  {
    j += 2;
    data->precision = ft_get_arg_int(va, ft_atoi(data->expr + j));
    return;
  }
  if (data->expr[i + 1] == '*')
    while (--i)
      if (data->expr[i] == '*')
        data->precision++;
  data->precision = ft_get_arg_int(va, data->precision);
}

// recupere la precison .. si nul .. verifie si *
void ft_get_precision(t_map *data, va_list va)
{
  int i;

  i = ft_strlen(data->expr);
  data->precision = 1;
  while (--i && data->expr[i] != '.') // trouve le dernier point
    ;
  if (data->expr[i] != '.')
    return ;
  if (data->expr[i + 1] == '*' && data->expr[i] == '.')
  {
    ft_update_precision(data, va);
    return ;
  }
  data->precision = ft_atoi(data->expr + i + 1); // +1 skip le '.'
}