#include "ft_printf.h"

static char hex_digit(int v)
{
  if (v >= 0 && v < 10)
    return '0' + v;
  else
    return 'a' + v - 10;
}

static void print_address_hex(void *p0, char *str)
{
  int i;
  int j;
  uintptr_t p;

  p = (uintptr_t)p0;
  i = 0;
  j = (sizeof(p) << 3);
  while (i < j && p >> i)
  {
    *str++ = hex_digit((p >> i) & 0xf);
    i += 4;
  }
  *str++ = 'x';
  *str++ = '0';
}

void ft_print_adress(t_map *data, void *p0)
{
  if (!(data->render = ft_strnew(16)))
    return;
  print_address_hex(p0, data->render);
  ft_strrev(data->render);
}