#include "ft_printf.h"

char *ft_strinser(char *str, char *cplm, size_t n) // ---------- INSERTION
{
  size_t i;
  char *dst;
  size_t t_size;

  i = -1;
  if (!cplm) // si complement null rien a faire
    return (str);
  t_size = ft_strlen(str) + ft_strlen(cplm);
  if (!(dst = ft_strnew(t_size)))
    return (NULL);
  while (++i < t_size)
    if (i < n || !*cplm)
      dst[i] = *str++;
    else
      dst[i] = *cplm++;
  return (dst);
}

void ft_offset_position(t_map *data)
{
  data->position--;
}

void ft_offset_position_i(t_map *data, int a)
{
  data->position = a;
}

int ft_str_pattern(const char *str, const char *find)
{
  int i;
  int j;

  i = 0;
  j = 0;
  if (find[0] == '\0')
    return (-1);
  while (str[i] != find[j] && str[i])
  {
    if (find[j] == '/' && ft_isdigit(str[i]))
      break ;
    i++;
  }
  while (str[i])
  {
    while (str[i++] == find[j++] && str[i] && find[j])
      ;
    while (find[j] == '/' && str[i] && ft_isdigit(str[i]))
      i++;
    if (find[j + 1] == '\0')
      return (i - j);
    i = i - j + 1;
    j = 0;
  }
  return (-1);
}