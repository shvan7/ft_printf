#include "ft_printf.h"

static char hexa_digit(unsigned long long v, int boolean)
{
  char t;

  t = boolean ? 'A' : 'a';
  if (v < 10)
    return ('0' + v);
  else
    return (t + v - 10);
}

static void ft_convert_x(unsigned long long nb, char *render, int boolean)
{
  int i;
  int j;

  i = 0;
  j = (sizeof(nb) << 3);
  while (i < j && nb >> i)
  {
    *render++ = hexa_digit((nb >> i) & 15, boolean);
    i += 4;
  }
}

void ft_print_x(t_map *data, unsigned long long nb, int boolean)
{
  int len;

  if (!(data->render = ft_strnew(20)))
    return;
  if (ft_strstr(data->input, "hh"))
    nb = (unsigned short)nb;
  else if (ft_strstr(data->input, "h"))
    nb = (unsigned char)nb;
  else if (ft_strstr(data->input, "ll")) // already u long long
    ;
  else if (ft_strstr(data->input, "l"))
    nb = (unsigned long)nb;
  else
    nb = (unsigned int)nb;
  ft_convert_x(nb, data->render, boolean);
  len = ft_strlen(data->render);
  if (nb != 0 && ft_strcspn(data->expr, "#") > -1)
  {
    data->render[len] = boolean ? 'X' : 'x';
    data->render[len + 1] = '0';
  }
  if (!data->render[0] && ft_strcspn(data->expr, ".") == -1)
    data->render[0] = '0';
  ft_strrev(data->render);
}