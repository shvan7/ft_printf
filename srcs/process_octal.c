#include "ft_printf.h"

static char octal_digit(unsigned long long v)
{
  if (v < 8)
    return ('0' + v);
  else
    return ('0' + v - 10);
}

static int ft_convert_o(unsigned long long nb, char *render)
{
  int i;
  int j;

  i = 0;
  j = (sizeof(nb) << 3);
  printf("== %d\n", (10 & 7));
  while (i < j && nb >> i)
  {
    *render++ = octal_digit((nb >> i) & 7);
    i += 3;
  }
  return (*render == '0');
}

void ft_print_o(t_map *data, unsigned long long nb)
{
  int ret;

  if (!(data->render = ft_strnew(12)))
    return;
  if (ft_strstr(data->input, "hh"))
    nb = (unsigned short)nb;
  else if (ft_strstr(data->input, "h"))
    nb = (unsigned char)nb;
  else if (ft_strstr(data->input, "ll")) // already u long long
    ;
  else if (ft_strstr(data->input, "l"))
    nb = (unsigned long)nb;
  else
    nb = (unsigned int)nb;
  ret = ft_convert_o(nb, data->render);
  if (!ret && ft_strcspn(data->expr, "#") > -1)
    data->render[ft_strlen(data->render)] = '0';
  ft_strrev(data->render);
}