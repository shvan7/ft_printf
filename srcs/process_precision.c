#include "ft_printf.h"

static void ft_p_str(char *render, int precision)
{
  int len;

  len = ft_strlen(render);
  render += precision;
  ft_bzero(render, len - precision);
}

static char *ft_p_int(char *render, int precision)
{
  char *tmp;
  int size;
  int diff;

  size = ft_strlen(render);
  tmp = render;
  diff = precision - size;
  
  if (diff > 0)
  {
    if (!(render = ft_strnew(precision)))
      return (NULL);

    while (size--)
      render[size + diff] = tmp[size];
    while (diff-- > -1)
      render[diff] = '0';
    ft_strdel(&tmp);
  }
  return (render);
}

// ONLY FOR "s && diouxX"             -- TODOS --
void ft_add_precision(t_map *data)
{
  char *tmp;

  tmp = data->render;
  if (data->output == 's')
    ft_p_str(data->render, data->precision);
  else
  {
    data->render = ft_p_int(data->render, data->precision);
  }
  printf("==> %s\n", tmp);
}



// if (!(precision) && ft_strcmp(render, "0")) // si render et precision = 0 -> render = null
// {
//   if (ft_strcspn(expr, "ouxX") > -1)
//     ft_bzero(render, size);
//   return;
// }