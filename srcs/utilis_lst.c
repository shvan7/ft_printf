#include "ft_printf.h"

// boucle sur la liste
void ft_forlst(t_map *data, void (*f)(t_map *e))
{
  if (data && f)
  {
    ft_star_of_list(&data); // return au debut
    while (data)
    {
      (*f)(data);
      data = data->next;
    }
  }
}

// boucle sur la liste sans revenir au debut
void ft_forlsts(t_map *data, void (*f)(t_map *e))
{
  if (data && f)
  {
    // return pas debut
    while (data)
    {
      (*f)(data);
      data = data->next;
    }
  }
}

// boucle sur la liste - pour update la position
void ft_forlsti(t_map *data, int a, void (*f)(t_map *e, int a))
{
  if (data && f)
  {
    // return pas debut
    while (data)
    {
      (*f)(data, a++);
      data = data->next;
    }
  }
}

// boucle sur la liste (utilise format)
void ft_forlstm(t_map *data, const char *format, void (*f)(t_map *e, const char *format))
{
  if (data && format && f)
  {
    ft_star_of_list(&data); // return au debut
    while (data)
    {
      (*f)(data, format);
      data = data->next;
    }
  }
}

// boucle sur la liste (utilise format)
void ft_forlst_arg(t_map *data, va_list va, void (*f)(t_map *e, va_list va))
{
  if (data && va && f)
  {
    ft_star_of_list(&data); // return au debut
    while (data)
    {
      (*f)(data, va);
      data = data->next;
    }
  }
}