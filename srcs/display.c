#include "ft_printf.h"

// affiche le resultat (en remplacant expr par render)
int ft_display(t_map *data, const char *format)
{
  int i;
  int ret;

  i = -1;
  ret = 0;
  if (data)
    ft_star_of_list(&data);
  while (format[++i])
  {
    if (data && data->render && i == data->info[0])
    {
      ret += ft_strlen(data->render);
      ft_putstr(data->render);
      i = data->info[1];
      data = data->next;
    }
    else
    {
      ft_putchar(format[i]);
      ret++;
    }
  }
  return (ret);
}

// // affiche les infos
void  ft_display_links(t_map *data)
{
  if (data)
  {
    // printf("next : %14p\t", data->next);
    // printf("prev : %14p\t", data->prev);
    
    printf("position : %-2d\t", data->position);
    printf("padd : %-2d\t", data->padding);
    printf("preci : %-2d\t", data->precision);
    
    // printf("[star : %-3d - end : %-3d]\t", data->info[0], data->info[1]);
    
    printf("[in : (%2s)\t", data->input);
    printf("out : (%2c)]\t", data->output);
    printf("prefix : (%s)\t", data->prefix);
    printf("expr : (%s)\n", data->expr);
    data = data->next;
  }
}