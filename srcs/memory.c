#include "ft_printf.h"

void  ft_star_of_list(t_map **data)
{
  while ((*data)->prev)
    *data = (*data)->prev;
}

void	ft_lstadd_end(t_map **data, t_map *new)
{
	if (data && *data && new)
	{
    new->prev = *data;
		(*data)->next = new;
    *data = new;
	}
}

// TODO : a revoir => pour fonctionner avec (ft_forlst)
// clean un maillon de la list
void ft_clean_links_list(t_map *data)
{
  if (data && data->expr)
    ft_strdel(&data->expr);
  if (data)
  {
    free (data); // sur une ligne ?
    data = NULL;
  }
}

// creer notre list
t_map *ft_build_links_list(const char *format, int position)
{
  int len;
  t_map *data;

  if (!(data = (t_map *)malloc(sizeof(t_map))))
    return (NULL);
  data->output = '\0';
  data->input = NULL;
  data->padding = 0;
  data->precision = 0;
  data->position = position;
  data->info[0] = ft_expr_index_star(format, position);
  data->info[1] = ft_expr_index_end(format, position);
  len = data->info[1] - data->info[0] + 1;
  if (!(data->expr = ft_strnew(len)))
    return (NULL);
  data->prefix = NULL; // pour le padding
  data->render = NULL; // chaine de sortie
  data->next = NULL;
  data->prev = NULL;
  return (data);
}

// construit la liste (liste complete avec maillon)
t_map *ft_build_lst(const char *format)
{
  int i;
  int nb_expr;
  t_map *data;
  t_map *new;

  i = 1;
  new = NULL;
  nb_expr = ft_nb_expr(format); // compte le nb expr
  if (!nb_expr)
    return (NULL);
  if (!(data = ft_build_links_list(format, i))) // creer le premier maillon
    return (NULL);
  while (++i <= nb_expr)
  {
    if (!(new = ft_build_links_list(format, i))) // creer les maillon suivant
      return (NULL);
    ft_lstadd_end(&data, new);
  }
  return (new ? new : data);
}