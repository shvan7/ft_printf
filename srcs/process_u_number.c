#include "ft_printf.h"

static int ft_countdec_printf(unsigned long long nb)
{
  int count;

  count = 1;
  while (nb /= 10)
    count++;
  return (count);
}

static void ft_build_str(unsigned long long n, char *p)
{
  if (n == 0)
    return;
  *p = n % 10 + 48;
  return (ft_build_str(n / 10, p - 1));
}

static char *ft_itoa_printf(unsigned long long n)
{
  unsigned int len;
  char *p;

  len = ft_countdec_printf(n);
  if (!(p = ft_strnew(len)))
    return (NULL);
  if (n == 0)
    return (p);
  ft_build_str(n, p + len - 1);
  return (p);
}

void  ft_print_u(t_map *data, unsigned long long d)
{
  if (ft_strstr(data->input, "hh"))
    d = (unsigned short)d;
  else if (ft_strstr(data->input, "h"))
    d = (unsigned char)d;
  else if (ft_strstr(data->input, "ll")) // already long long
    ;
  else if (ft_strstr(data->input, "l"))
    d = (unsigned long)d;
  else
    d = (unsigned int)d;
  data->render = ft_itoa_printf(d);
}