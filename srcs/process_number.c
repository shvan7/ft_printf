#include "ft_printf.h"

static int ft_countdec_printf(long long nb)
{
  int count;

  count = 1;
  while (nb /= 10)
    count++;
  return (count);
}

static void ft_build_str(long long n, char *p)
{
  if (n == 0)
    return;
  *p = -(n % 10) + 48;
  return (ft_build_str(n / 10, p - 1));
}

static char *ft_itoa_printf(long long n)
{
  unsigned int len;
  char *p;

  len = n < 0 ? ft_countdec_printf(n) + 1 : ft_countdec_printf(n);
  if (!(p = ft_strnew(len)))
    return (NULL);
  if (n < 0)
    p[0] = '-';
  
  if (n == 0 && (p[0] = '0'))
    return (p);
  ft_build_str(n > 0 ? -n : n, p + len - 1);
  return (p);
}

void  ft_print_d(t_map *data, long long d)
{
  if (ft_strstr(data->input, "hh"))
    d = (short)d;
  else if (ft_strstr(data->input, "h"))
    d = (char)d;
  else if (ft_strstr(data->input, "ll")) // already long long
    ;
  else if (ft_strstr(data->input, "l"))
    d = (long)d;
  else
    d = (int)d;
  data->render = ft_itoa_printf(d);
}