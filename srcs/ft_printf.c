#include "ft_printf.h"

// function principal
int ft_printf(const char *format, ...)
{
  va_list va;
  int ret;
  va_start(va, format);

  t_map *data;
  if (!(data = ft_build_lst(format))) // build et rempli la lst
  {
    ret = ft_display(data, format);
    // printf("ret = %d\n", ret);
    return (ret);
  }

  // RECUPERATION
  ft_forlstm(data, format, ft_expr_fill); // recupere les expr
  ft_forlst(data, ft_get_flag_input); // recupere les flag input
  ft_forlst(data, ft_get_flag_output); // recupere le flag de sorti
  
  // MODIFIE POSITION SELON SPEC FLAG
  ft_forlst(data, ft_update_positon); // selection de l'arg
  
  // RECUPERE PADDING ET PRECISION
  ft_forlst_arg(data, va, ft_get_padding);
  ft_forlst_arg(data, va, ft_get_precision);

  // TRAITE LE RENDER
  ft_forlst_arg(data, va, ft_process_render);
  ft_forlst(data, ft_add_precision); // ajoute la precision
  ft_forlst(data, ft_add_padding); // ajoute le padding

  // VIEW
  // printf infos
  ft_forlst(data, ft_display_links); // affiche les infos1
  va_end(va);
  ret = ft_display(data, format);
  // printf("ret = %d\n", ret);
  return (ret);
}