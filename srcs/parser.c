#include "ft_printf.h"

// check les charactere d'arret de l'expression
int ft_check_end_expr(char c)
{
  int count;
  char cc;

  cc = c;
  count = 0;
  if (!cc)
    return (1);
  if (!ft_isdigit(c)) // => si pas un chiffre
    count++;
  if (ft_strcspn(F_INPUT, &cc) == -1) // => si pas un flag input
    count++;
  if (ft_strcspn(F_SPEC, &cc) == -1) // => si pas un flag special
    count++;
  if (ft_strcspn(".", &cc) == -1) // => si pas un point
    count++;
  return (count == 4);
}

// compte le nombre d'expr dans format
int ft_nb_expr(const char *format)
{
  int i;
  int boolean;
  int count;

  i = -1;
  boolean = 0;
  count = 0;
  while (format[++i])
  {
    if (!boolean && format[i] == '%')
      ++boolean && ++count && ++i; // increment les deux
    if (!format[i])
      return (count);
    if (boolean && ft_check_end_expr(format[i]))
      boolean = 0;
  }
  return (count);
}

// return la size de la eme(index) expression
int ft_expr_index_star(const char *format, int index)
{
  int i;
  int boolean;
  int count;

  i = -1;
  boolean = 0;
  count = 0;
  while (format[++i])
  {
    if (!boolean && format[i] == '%') // si pas trouve de % et que la valeur et % alors new expr
      ++boolean && ++i && ++count;
    if (count == index)
      return (i - 1);
    if (boolean && ft_check_end_expr(format[i])) // si tu traitre une expr et que tu tombes sur un end
      boolean = 0;
  }
  return (-1);
}

// return la size de la eme(index) expression
int ft_expr_index_end(const char *format, int index)
{
  int i;
  int boolean;
  int count;

  i = -1;
  boolean = 0;
  count = 0;
  while (format[++i])
  {
    if (!boolean && format[i] == '%') // si pas trouve de % et que la valeur et % alors new expr
      ++boolean && ++i && ++count;
    if (boolean && ft_check_end_expr(format[i])) // si tu traitre une expr et que tu tombes sur un end
    {
      if (count == index)
        return (format[i] ? i : --i); // size a une valeur et on est pas en fin de chaine ajouter 1
      boolean = 0;
    }
  }
  return (count == index ? i - 1 : -1);
}

// rempli expr
void ft_expr_fill(t_map *data, const char *format)
{
  int len;

  len = data->info[1] - data->info[0] + 1;
  ft_strncpy(data->expr, format + data->info[0], len); // avance format a expr
}