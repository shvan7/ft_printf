#include "ft_printf.h"

void ft_print_bin(t_map *data, long long nb)
{
  int i;
  int j;
  int bit;

  i = 0;
  j = -1;
  bit = (sizeof(nb) << 3);
  if (!(data->render = ft_strnew(65)))
    return ;
  while (i < bit)
  {
    if (!(i % 4))
      data->render[++j] = ' ';
    data->render[++j] = (char)((nb >> i) & 1) + '0';
    i += 1;
    if (!(nb >> i) && !(i % 4))
      break ;
  }
  ft_strrev(data->render);
}