# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/12 10:45:11 by aulima-f          #+#    #+#              #
#    Updated: 2018/11/23 19:47:46 by aulima-f         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################ STYLE ############################################
GREEN = \033[0;32m
WHITE = \033[1;37m
BLUE = \033[1;34m
RED = \033[0;31m
YELLOW = \033[1;33m

CC = gcc
AR = ar rc
RAN = ranlib
FLAGS = -Wall -Wextra -Werror
NAME = libftprintf.a

OKGREEN = $(YELLOW)\t=======> $(GREEN)[OK]$(WHITE)
KORED = $(YELLOW)\t=======> $(RED)[error]$(WHITE)

#*********************************************************************************

FUNC = 			ft_printf.c\
						\
						parser.c\
						memory.c\
						\
						get_info.c\
						get_arg.c\
						get_position.c\
						get_precision.c\
						get_padding.c\
						\
						process_padding.c\
						process_precision.c\
						\
						process_number.c\
						process_u_number.c\
						process_octal.c\
						process_hexa.c\
						process_adress.c\
						process_binary.c\
						process_string.c\
						\
						utilis_lst.c\
						utilis.c\
						\
						render.c\
						display.c\

FUNC_LIB = 	ft_atoi.c\
						ft_isdigit.c\
						ft_putchar.c\
						ft_putchar_fd.c\
						ft_bzero.c\
						ft_putstr.c\
						ft_strcpy.c\
						ft_strcspn.c\
						ft_strdel.c\
						ft_strdup.c\
						ft_strncpy.c\
						ft_strnew.c\
						ft_strrev.c\
						ft_strstr.c\
						ft_putstr_fd.c\
						ft_swap.c\
						ft_strcmp.c\
						ft_strlen.c\
						ft_abs.c\

#********************************FOLDER********************************************
DIR_HEADERS = includes/

DIR_SRCS_LIB = libft/
DIR_SRCS = srcs/

DIR_OBJECT = objs/

#********************************FILE**********************************************

OBJECT = $(FUNC:.c=.o)
OBJECT_LIB = $(FUNC_LIB:.c=.o)

FILES_OBJECT = $(addprefix $(DIR_OBJECT), $(OBJECT))
FILES_OBJECT_LIB = $(addprefix $(DIR_OBJECT), $(OBJECT_LIB))

all: $(NAME)

$(DIR_OBJECT)%.o: $(DIR_SRCS)%.c $(DIR_HEADERS)
	@$(CC) $(FLAGS) -c -o $@ $< -I$(DIR_HEADERS)

$(DIR_OBJECT)%.o: $(DIR_SRCS_LIB)%.c $(DIR_HEADERS)
	@$(CC) $(FLAGS) -c -o $@ $< -I$(DIR_HEADERS)

$(NAME): $(FILES_OBJECT) $(FILES_OBJECT_LIB) Makefile
	@$(AR) $@ $(FILES_OBJECT_LIB) $(FILES_OBJECT)
	@$(RAN) $@
	@echo "- $(BLUE)Create lib printf$(OKGREEN)"

clean:
	@rm -f $(FILES_OBJECT) && echo "- $(BLUE)Delete ObjProg$(OKGREEN)"
	@rm -f $(FILES_OBJECT_LIB) && echo "- $(BLUE)Delete ObjLibft$(OKGREEN)"

fclean: clean
	@rm -f $(NAME) && echo "- $(BLUE)Delete libPrintf$(OKGREEN)"

cmp: fclean $(NAME)
	@$(CC) $(FLAGS) main.c $(NAME)
	@./a.out

push:
	@git add -A
	@git commit -m '${MSG}s'
	@git push origin master && echo "- $(BLUE)PUSH OK ON $(WHITE)===> $(YELLOW)`git config remote.origin.url`"

re: fclean all

.PHONY: clean fclean all