/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 07:31:46 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:42:07 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <stdio.h>

void		ft_strdel(char **as)
{
	printf("===> truc\n");
	if (!as)
		return ;
	free((void *)*as);
	*as = NULL;
}
