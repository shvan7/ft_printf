/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 07:06:46 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:31:36 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strnew(size_t size)
{
	char *p;

	if (!(p = (char *)malloc((size + 1) * sizeof(char))))
		return (NULL);
	ft_bzero(p, size + 1);
	return (p);
}
