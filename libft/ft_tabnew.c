/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 14:13:15 by aulima-f          #+#    #+#             */
/*   Updated: 2019/04/29 14:40:58 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		**ft_tabnew(int column, int nb)
{
	int **tab;

	if (!(tab = (int **)malloc(sizeof(int *) * column)))
		return (NULL);
	while (column--)
		if (!(tab[column] = (int *)malloc(sizeof(int) * nb)))
			return (NULL);
	return (tab);
}
