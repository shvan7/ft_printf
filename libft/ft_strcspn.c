/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcspn.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 08:06:23 by aulima-f          #+#    #+#             */
/*   Updated: 2018/12/05 12:12:45 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

int		ft_strcspn(char const *s, char const *charset)
{
	int i;

	if (s && charset)
		while (*charset)
		{
			i = -1;
			while (s[++i])
				if (s[i] == *charset)
					return (i);
			charset++;
		}
	return (-1);
}
