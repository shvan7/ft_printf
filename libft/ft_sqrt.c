/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/26 14:12:33 by aulima-f          #+#    #+#             */
/*   Updated: 2019/04/26 14:13:57 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_sqrt(int c)
{
	int nbr;

	nbr = ft_power(10, ft_countdec(c) / 2) + 100;
	while (--nbr > 1)
	{
		if (c == (nbr * nbr))
			return (nbr);
		if (c > (nbr * nbr))
			return (nbr + 1);
	}
	return (0);
}
