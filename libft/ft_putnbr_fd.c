/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/15 11:25:41 by aulima-f          #+#    #+#             */
/*   Updated: 2018/11/20 14:44:13 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_print_rec(unsigned int n, int fd)
{
	if (!n)
		return ;
	ft_print_rec(n / 10, fd);
	ft_putchar_fd(n % 10 + 48, fd);
}

void			ft_putnbr_fd(int n, int fd)
{
	unsigned int nb;

	nb = ft_abs(n);
	if (n < 0)
		ft_putchar_fd('-', fd);
	if (!nb)
		ft_putchar_fd('0', fd);
	ft_print_rec(nb, fd);
}
