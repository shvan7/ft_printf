/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_remchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 12:08:54 by aulima-f          #+#    #+#             */
/*   Updated: 2018/12/05 12:09:20 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

int		ft_remchar(char *str, char c, size_t n)
{
	char	*src;
	int		i;

	i = 0;
	if (!str)
		return (0);
	src = str;
	while (n--)
	{
		*str = *src;
		if (*str != c)
		{
			str++;
			i++;
		}
		src++;
	}
	*str = '\0';
	return (i);
}
