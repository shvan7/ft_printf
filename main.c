#include "includes/ft_printf.h"

int main(void)
{
  printf("\n========================= VRAI\n");

  printf("%.2s is a string", "this");
  printf("\n");
  printf("%.10d", 123);
  printf("\n");

  printf("\n========================= FAUX\n");

  ft_printf("%.2s is a string", "this");
  printf("\n");
  ft_printf("%.10d", 123);
  printf("\n");

  return (0);
}
